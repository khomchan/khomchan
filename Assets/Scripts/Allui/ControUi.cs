﻿using System;
using Allscripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scrip.AllUI
{
    public class ControUi : MonoBehaviour
    {
        
        [SerializeField] private GameObject UIStart;
        [SerializeField] private GameObject UIPlay;
        [SerializeField] private GameObject UIend;
        [SerializeField] private GameObject level2;
        [SerializeField] private GameObject textend;
        [SerializeField] private GameObject buttonEnd;
        public Text ScoreEndGame;
        public Text ScoreonPlay;
        private bool textClose;

        public void Start()
         {
             GameManager.Instance.Music();
             GameManager.Instance.Sethponstart();
             UIPlay.gameObject.SetActive(false);
            UIend.gameObject.SetActive(false);
            GameManager.Instance.player.gameObject.SetActive(false);
            GameManager.Instance.enemy.gameObject.SetActive(false);
            level2.gameObject.SetActive(false);
            textClose = false;
         }

         public void Playgame()
         {
             UIend.gameObject.SetActive(false);
             UIPlay.gameObject.SetActive(true);
             UIStart.gameObject.SetActive(false);
             Debug.Log("play");
             GameManager.Instance.player.gameObject.SetActive(true);
             GameManager.Instance.enemy.gameObject.SetActive(true);
         }

         private void EndGame()
         {
             ScoreEndGame.text = $"Score:{ScoreManager.Instance.Score}";
             UIend.gameObject.SetActive(true);
             GameManager.Instance.player.gameObject.SetActive(false);
             GameManager.Instance.enemy.gameObject.SetActive(false);
             if (textClose==true)
             {
                 textend.gameObject.SetActive(true);
                 buttonEnd.gameObject.SetActive(false);
             }
             else
             {
                 textend.gameObject.SetActive(false);
                 buttonEnd.gameObject.SetActive(true);
             }
         }

         public void Exit()
         {
             Application.Quit();
         }

         public void Menu()
         {
             textClose = false;
             GameManager.Instance.resetPosition();
             ScoreManager.Instance.Resetscore();
             GameManager.Instance.SetHPonrestart();
             GameManager.Instance.player.lv2 = false;
             Mainmanu();
         }

         public void Reset()
         {
             textClose = false;
             GameManager.Instance.resetPosition();
             GameManager.Instance.SetHPonrestart();
             ScoreManager.Instance.Resetscore();
             GameManager.Instance.player.lv2 = false;
             Playgame();
             
         }

         public void Mainmanu()
         {
             GameManager.Instance.Music();
             UIPlay.gameObject.SetActive(false);
             UIend.gameObject.SetActive(false);
             GameManager.Instance.player.gameObject.SetActive(false);
             GameManager.Instance.enemy.gameObject.SetActive(false);
             UIStart.gameObject.SetActive(true);
         }

         public void Lv2()
         {
             level2.gameObject.SetActive(true);
             UIend.gameObject.SetActive(false);
             GameManager.Instance.SetHPonrestart();
             textClose = true;
             GameManager.Instance.player.gameObject.SetActive(true);
             GameManager.Instance.enemyA.gameObject.SetActive(true);
             GameManager.Instance.enemyB.gameObject.SetActive(true);
             GameManager.Instance.player.gameObject.transform.position = GameManager.Instance.player.Positionplayer;
             

         }
         

         private void Update()
         {
             ScoreonPlay.text = $"Score : {ScoreManager.Instance.Score}";
             if (GameManager.Instance.enemy.hp<=0)
             {
                 GameManager.Instance.player.lv2 = true;
                 EndGame();
             }

             if (GameManager.Instance.player.hp<=0)
             {
                 EndGame();
             }

             if (GameManager.Instance.enemyA.hp<=0&&GameManager.Instance.enemyB.hp<=0)
             {
                 EndGame();
             }

         }
         
        
    }
}
