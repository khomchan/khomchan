﻿
using System;
using UnityEngine;


namespace Allscripts
{
    public class Player:Allplayer

    {
        [SerializeField] private AudioClip playerloss;
        [SerializeField] private Bullet bullet;
        [SerializeField] private Enemy enemy;
        internal Vector3 Positionplayer;
        internal bool lv2=false; 
        
        
        public Player(int hp, int damage, GameObject gameObject) : base(hp, damage, gameObject)
        {
        }
        void LoosHP()
        {
            hp = hp - enemy.damage;
            if (hp<=0)
            {
                AudioSource.PlayClipAtPoint(playerloss,Camera.main.transform.position,0.2f);
                gameObject.SetActive(false);
                gameObject.transform.position =Positionplayer;
                enemy.gameObject.transform.position = enemy.Resetenamy;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            LoosHP();
        }

        private void Start()
        {
            var Originalposition =gameObject.transform.position;
            Positionplayer=new Vector3(Originalposition.x,Originalposition.y,Originalposition.z );
            
        }
        public override void Attack()
        {
            if (lv2==false)
            {
                damage = 1;
                bullet.Strike();
            }
            if (lv2)
            {
                damage = 2;
                bullet.StrikeLv2();
                
            }
        }
        
    }
}