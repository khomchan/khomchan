﻿using UnityEngine;

namespace Allscripts
{
    public class Allplayer:MonoBehaviour
    {
        [SerializeField] public int hp ;
        [SerializeField] public int damage;
        [SerializeField] private GameObject Object;
        public Allplayer(int hp, int damage,GameObject gameObject)
        {
            this.hp = hp;
            this.damage = damage;
            Object = gameObject;
        }

        public virtual void Attack()
        {
            
        }
    }
} 


