﻿using System;
using UnityEngine;
using UnityEngine.Serialization;


namespace Allscripts
{
    public class Enemy:Allplayer

    {
        [SerializeField] private AudioClip ememyloos;
        [SerializeField] private AudioClip ememystrike;
        [SerializeField] private double SpeedBullet;
        [FormerlySerializedAs("Playership")] [SerializeField] internal Player playerAtk; 
        [SerializeField] private Bullet Bulletenemy;
        internal Vector3 Resetenamy;
        private float Cooldown;
        public Enemy(int hp, int damage, GameObject gameObject,Player playerATK) : base(hp, damage, gameObject)
        {
            playerAtk = playerATK;
        }

        void LoosHP()
        {
            hp = hp - playerAtk.damage;
            if (hp<=0)
            {
                AudioSource.PlayClipAtPoint(ememyloos,Camera.main.transform.position, 0.2f);
                gameObject.SetActive(false);
                gameObject.transform.position = Resetenamy;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            LoosHP();
            ScoreManager.Instance.Counterscore();
        }

        private void Start()
        {
            var position = gameObject.transform.position;
            Resetenamy=new Vector3(position.x,position.y,position.z);
        }

        private void Update()
        {
            Cooldown += Time.deltaTime;
            if (gameObject==true)
            {
                if (Cooldown>=SpeedBullet)
                {
                    AudioSource.PlayClipAtPoint(ememystrike,Camera.main.transform.position,0.1f);
                    Bulletenemy.EnemyStrike();
                    Cooldown = 0;
                }
            }
            
            
            
        }
    }
}

   