﻿
using UnityEngine;
using UnityEngine.Serialization;

namespace Allscripts
{
    
    public class Bullet:MonoBehaviour

    {
            [SerializeField] private Rigidbody bulletLv2;
            [SerializeField] private AudioClip playerstrike;
            public Rigidbody Gunbullet;
            [SerializeField] private float speedStrike;
            public void Strike()
            {
                AudioSource.PlayClipAtPoint(playerstrike,Camera.main.transform.position,0.2f);
                Rigidbody BulletCopy = Instantiate(Gunbullet, transform.position, transform.rotation);
                BulletCopy.AddForce(0,speedStrike,0);

            }
            public void EnemyStrike()
            {
                Rigidbody BulletCopy = Instantiate(Gunbullet, transform.position, transform.rotation);
                BulletCopy.AddForce(0,-speedStrike,0);

            }
            public void StrikeLv2()
            {
                AudioSource.PlayClipAtPoint(playerstrike,Camera.main.transform.position,0.2f);
                Rigidbody BulletCopy = Instantiate(bulletLv2, transform.position, transform.rotation);
                BulletCopy.AddForce(0,700,0);

            }
        }
}
