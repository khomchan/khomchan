﻿using System.Collections;
using System.Collections.Generic;
using Allscripts;
using GameManage;
using UnityEngine;

public class GameManager : MonoSingleton<GameManager>
{
    internal int Hpenemy;
    internal int Hpplayer;
    private AudioSource Playmusic;
    [SerializeField] private AudioClip Startmusic;
    [SerializeField] internal Player player;
    [SerializeField] internal Enemy enemy;
    [SerializeField] internal Enemy enemyA;
    [SerializeField] internal Enemy enemyB;
    public void Sethponstart()
    {
        Hpenemy = enemy.hp;
        Hpplayer = player.hp;
        Debug.Log("SetHP");
    }
    
    public void SetHPonrestart()
    {
        enemy.hp = Hpenemy;
        enemyA.hp = Hpenemy;
        enemyB.hp = Hpenemy;
        player.hp = Hpplayer;
        Debug.Log("ReHP");
        
    }

    public void Music()
    {
        Playmusic = GetComponent<AudioSource>();
        Playmusic.clip = Startmusic;
        Playmusic.loop = true;
        Playmusic.volume = 0.3f;
        Playmusic.Play();
    }

    public void resetPosition()
    {
        player.transform.position = player.Positionplayer;
        enemy.transform.position = enemy.Resetenamy;
        enemyA.transform.position = enemyA.Resetenamy;
        enemyB.transform.position = enemyB.Resetenamy;
    }
    
    
}

